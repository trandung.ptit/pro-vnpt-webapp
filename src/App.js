import { Layout, Menu, Row, Col, Space } from 'antd';
import {
  BarsOutlined,
  ContactsOutlined
} from '@ant-design/icons';
import 'antd/dist/antd.css'
import './App.css';
import { Route, Switch, Link } from 'react-router-dom'

import { useState } from 'react';
import ComingSoon from './views/category/coming-soon';
import SendImessDetail from './views/category/send-imess-detail';
import Template from './views/category/template';
import SendMultipleImess from './views/category/send-multiple-imess';
import SendMultipleImessDetail from './views/category/send-multiple-imess-detail';
import TemplateDetail from './views/category/template-detail';
import SearchSMS from './views/category/search-sms';

const {  Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const App = () => {

  const [collapsed, setCollapsed] = useState(false);

  const AppName = () => {
    return (
      <Col><span style={{ fontSize: "15px", color: "white" }}></span></Col>
    )
  }

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible defaultCollapsed={false} collapsed={collapsed} onCollapse={(collapsed) => setCollapsed(collapsed)}>
        <div className="logo" >

          <Row justify="center" align="middle">

            <Space>
            <Col><ContactsOutlined style={{ fontSize: "25px", color: "white" }} /></Col>
            {collapsed ? null : <AppName />}
            </Space>
          </Row>
        </div>
        <Menu theme="dark" defaultOpenKeys={['sub1']} defaultSelectedKeys={['1']} mode="inline">
          <SubMenu key="sub1" icon={<BarsOutlined />} title="Auto Imess">
            {/* <Menu.Item key="2">
              <Link to="/send-sms">Gửi tin nhắn</Link>
            </Menu.Item> */}
            <Menu.Item key="1">
              <Link to="/send-multiple-sms">Gửi nhiều tin nhắn</Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/search-sms">Tìm kiếm tin nhắn</Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/template">Cú pháp tin nhắn</Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Link to="/create-icloud">Tạo icloud</Link>
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Content style={{ margin: '5px 16px' }}>
          <Switch>
            <Route exact path="/search-sms">
              <SearchSMS/>
            </Route>
            <Route path="/template">
              <Template/>
            </Route>
            <Route path="/send-multiple-sms">
              <SendMultipleImess />
            </Route>
            <Route exact path="/">
              <SendMultipleImess />
            </Route>
            <Route exact path="/send-sms-detail/:id">
              <SendImessDetail />
            </Route>
            <Route exact path="/send-multiple-sms-detail/:id">
              <SendMultipleImessDetail />
            </Route>
            <Route exact path="/template-detail/:id">
              <TemplateDetail />
            </Route>
            <Route path="*">
            <ComingSoon/>
            </Route>
          </Switch>
        </Content>
      </Layout>
    </Layout>
  );

}

export default App;
