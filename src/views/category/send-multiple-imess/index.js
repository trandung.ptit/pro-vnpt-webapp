import React, { useEffect, useState } from "react";
import {
  notification,
  Button,
  Table,
  Space,
  Row,
  Col,
  Modal,
  Form,
  Input,
  Typography,
  Select,
  Tag,
} from "antd";
import axios from "axios";
import { host } from "../../../constant";
import {
  SyncOutlined,
  AppstoreAddOutlined,
  QrcodeOutlined,
  SisternodeOutlined,
} from "@ant-design/icons";
import { useLocation, useHistory } from "react-router";
import { Link } from "react-router-dom";
import QRCode from "react-qr-code";
const SendMultipleImess = () => {
  let query = new URLSearchParams(useLocation().search);
  let history = useHistory();
  const [customerCategories, setCustomerCategories] = useState([]);
  const [templates, setTemplates] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isDialogLoading, setIsDialogLoading] = useState(false);
  const [selectedId, setSelectedId] = useState();
  const [visible, setVisible] = useState(false);
  const [dialogVisible, seteDialogVisible] = useState(false);
  const [qrCodeModalVisible, setQrCodeModalVisible] = useState(false);
  const [isSplitTab, setIsSplitTab] = useState(false);

  const [confirmLoading, setConfirmLoading] = useState(false);
  const [paginationConfig, setPaginationConfig] = useState({
    total: 0,
    onChange: (current, size) => handleChangePage(current, size),
    current: 1,
  });
  const [form] = Form.useForm();
  const { Title } = Typography;
  const { TextArea } = Input;
  const { Option } = Select;

  const getAll = (page = 1, size = 10) => {
    setIsLoading(true);
    axios
      .get(
        `${host}/api/v1/multiple-content/getAll?page=${
          page - 1
        }&size=${size}&sort=id,desc`
      )
      .then((res) => {
        setCustomerCategories(res.data.items || []);
        setIsLoading(false);
        setPaginationConfig({
          total: res.data.totalItems,
          onChange: (current, size) => handleChangePage(current, size),
          current: parseInt(page),
        });
      })
      .catch((error) => {
        openNotificationWithIcon(
          "error",
          "Tải danh sách thất bại",
          error.message
        );
        setIsLoading(false);
      });
  };

  const getAllTemplate = () => {
    axios
      .get(`${host}/api/v1/template`)
      .then((res) => {
        setTemplates(res.data || []);
      })
      .catch((error) => {
        openNotificationWithIcon(
          "error",
          "Tải danh sách cú pháp thất bại",
          error.message
        );
      });
  };

  const callDelete = () => {
    axios
      .delete(`${host}/api/v1/multiple-content/delete/${selectedId}`)
      .then((res) => {
        openNotificationWithIcon("success", "Xoá thành công", "");
        getAll();
      })
      .catch((error) => {
        openNotificationWithIcon("error", "Xoá thất bại", error.message);
      });
    setVisible(false);
    setConfirmLoading(false);
  };

  const callPut = (data) => {
    setIsDialogLoading(true);
    var receivers_array = data.receivers.trim().split(/\n/);
    const receivers_result = [];
    receivers_array.forEach((e) =>
      receivers_result.push({
        phoneNumber: e.split("||")[0],
        smsContent: e.split("||")[1],
      })
    );
    var request = {
      templateId: data.templateId,
      smsDurationFrom: data.smsDurationFrom,
      smsDurationTo: data.smsDurationTo,
      receivers: receivers_result,
    };
    // neu gui chia tab thi them tabCount
    // neu la gui binh thuong thi them note vao
    if (isSplitTab) {
      request = {
        ...request,
        tabCount: data.tabCount,
        notes:data.note
      };
    } else {
      request = {
        ...request,
        multipleContent: {
          note: data.note,
        },
      };
    }
    const path = isSplitTab
      ? `${host}/api/v1/multiple-content/updateList`
      : `${host}/api/v1/multiple-content/update`;
    axios
      .put(path, request)
      .then((res) => {
        openNotificationWithIcon("success", "Thành công", "");
        getAll();
        seteDialogVisible(false);
        setIsDialogLoading(false);
        setIsSplitTab(false);
      })
      .catch((error) => {
        openNotificationWithIcon(
          "error",
          "Thất bại",
          error.response.data.message
        );
        seteDialogVisible(false);
        setIsDialogLoading(false);
      });
    form.resetFields();
  };

  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
      message: message,
      description: description,
    });
  };

  const renderTagColor = (text) => {
    switch (text) {
      case "SENDING":
        return "success";
      case "COMPLETED":
        return "blue";
      default:
        return "red";
    }
  };

  const renderTagText = (text) => {
    switch (text) {
      case "SENDING":
        return "Đang gửi";
      case "COMPLETED":
        return "Hoàn thành";
      default:
        return "Đang dừng";
    }
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "multipleContent.id",
      render: (text) => <a href="# ">{text}</a>,
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      render: (text) => (
        <Tag color={renderTagColor(text)} key={renderTagText(text)}>
          {renderTagText(text)}
        </Tag>
      ),
    },
    {
      title: "Thời gian chờ",
      dataIndex: "time",
      key: "time",
    },
    {
      title: "Người nhận",
      key: "totalReceiver",
      render: (text, record) => (
        <p>
          {record.totalReceived} / {record.totalReceiver}
        </p>
      ),
    },
    {
      title: "Thao tác",
      key: "action",
      render: (text, record) => (
        <Space size="small">
          <Button
            type="primary"
            onClick={() => {
              setSelectedId(record.id);
              showQRModal();
            }}
          >
            <QrcodeOutlined />
          </Button>
          <Link to={`/send-multiple-sms-detail/${record.id}`}>
            <Button
              type="primary"
              // onClick={() => {
              //   navigate(`/send-sms-detail/${record.id}`)
              // }}
              // href={`/send-sms-detail/${record.id}`}
            >
              Chi tiết
            </Button>
          </Link>
          <Button
            onClick={() => {
              setSelectedId(record.id);
              showModal();
            }}
            type="primary"
            danger
          >
            Xoá
          </Button>
        </Space>
      ),
    },
  ];

  const handleChangePage = (current, size) => {
    history.push({
      pathname: "/send-multiple-sms",
      search: `?page=${current}&size=${size}`,
    });
    getAll(current, size);
  };

  const showModal = () => {
    setVisible(true);
  };

  const showDialogModal = () => {
    seteDialogVisible(true);
  };

  const showQRModal = () => {
    setQrCodeModalVisible(true);
  };

  const handleOk = () => {
    // setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);
    callDelete();
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const handleDialogCancel = () => {
    seteDialogVisible(false);
    setIsSplitTab(false);
    form.resetFields();
  };

  const handleQROk = () => {
    setQrCodeModalVisible(false);
  };

  const handleQRCancel = () => {
    setQrCodeModalVisible(false);
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 11,
      span: 11,
    },
  };

  const onFinish = (values) => {
    callPut(values);
  };

  useEffect(() => {
    getAllTemplate();
    getAll(query.get("page") || 1, query.get("size") || 10);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div>
      <Row justify="start">
        <Title>Gửi tin nhắn</Title>
      </Row>
      <Row justify="end" style={{ paddingBottom: "10px" }}>
        <Col>
          <Space>
            <Button type="primary" onClick={() => showDialogModal()}>
              <AppstoreAddOutlined style={{ fontSize: "16px" }} /> Thêm
            </Button>
            <Button
              type="primary"
              onClick={() => {
                setIsSplitTab(true);
                showDialogModal();
              }}
            >
              <SisternodeOutlined style={{ fontSize: "16px" }} /> Thêm tự động chia
            </Button>
            <Button
              type="primary"
              onClick={() =>
                getAll(query.get("page") || 1, query.get("size") || 10)
              }
              disabled={isLoading}
            >
              <SyncOutlined style={{ fontSize: "16px" }} spin={isLoading} />
              Tải lại
            </Button>
          </Space>
        </Col>
      </Row>
      <Table
        rowKey="id"
        columns={columns}
        dataSource={customerCategories}
        pagination={paginationConfig}
        loading={isLoading}
      />

      {/* Modal Delete*/}
      <Modal
        title="Thông báo xác nhận"
        visible={visible}
        onOk={handleOk}
        okButtonProps={{ danger: true }}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        okText="Xoá"
        cancelText="Huỷ"
      >
        <p>Bạn có muốn xoá không</p>
      </Modal>

      {/* Modal QR CODE*/}
      <Modal
        title="QR Code"
        visible={qrCodeModalVisible}
        onOk={handleQROk}
        onCancel={handleQRCancel}
        cancelText="Huỷ"
      >
        <Row justify="center" align="middle">
          <Space>
            <Space>
              <QRCode
                value={`${host}/api/v1/multiple-content/getOneUncompleted/${selectedId}`}
              />
            </Space>
          </Space>
        </Row>
      </Modal>

      {/* Modal create  */}
      <Modal
        title="Gửi tin nhắn"
        visible={dialogVisible}
        onCancel={handleDialogCancel}
        footer={null}
        width={1000}
      >
        <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
          <Form.Item name="id" style={{ display: "none" }}>
            <Input />
          </Form.Item>
          <Form.Item
            name="templateId"
            label="Loại"
            rules={[{ required: true, message: "Không được để trống" }]}
          >
            <Select style={{ width: 120 }}>
              {templates.map((template, index) => {
                return (
                  <Option key={index} value={template.id}>
                    {template.name}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item label="Thời gian nhắn" required>
            <Input.Group compact>
              <Form.Item
                rules={[{ required: true, message: "Không được để trống" }]}
                name="smsDurationFrom"
                noStyle
              >
                <Input
                  style={{
                    width: 120,
                    textAlign: "center",
                  }}
                  placeholder="Thời gian từ"
                />
              </Form.Item>
              <Input
                className="site-input-split"
                style={{
                  width: 30,
                  pointerEvents: "none",
                }}
                placeholder="~"
                disabled
              />
              <Form.Item
                rules={[{ required: true, message: "Không được để trống" }]}
                name="smsDurationTo"
                noStyle
              >
                <Input
                  className="site-input-right"
                  style={{
                    width: 120,
                    textAlign: "center",
                  }}
                  placeholder="Thời gian đến"
                  rules={[{ required: true, message: "Không được để trống" }]}
                />
              </Form.Item>
            </Input.Group>
          </Form.Item>
          <Form.Item name="note" label="Ghi chú">
            <Input />
          </Form.Item>
          <Form.Item name="tabCount" label="Số lượng tab" hidden={!isSplitTab}>
            <Input />
          </Form.Item>
          <Form.Item
            name="receivers"
            label="Người nhận || Nội dung"
            rules={[{ required: true, message: "Không được để trống" }]}
          >
            <TextArea rows={10} />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit" loading={isDialogLoading}>
              Lưu
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default SendMultipleImess;
