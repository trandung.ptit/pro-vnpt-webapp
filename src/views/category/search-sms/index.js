import React, { useEffect, useState } from "react";
import {
  notification,
  Table,
  Tag,
  Row,
  Col,
  Typography,
  Input,
} from "antd";
import axios from "axios";
import { host } from "../../../constant";
const SearchSMS = () => {
  const [projecTypes, setProjectTypes] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const { Title } = Typography;
  const { Search } = Input;

  const findAllByPhoneNumber = (phoneNumber) => {
    setIsLoading(true);
    axios
      .get(`${host}/api/v1/receiver/findAllByPhoneNumber/${phoneNumber}`)
      .then((res) => {
        setProjectTypes(res.data || []);
        setIsLoading(false);
      })
      .catch((error) => {
        openNotificationWithIcon(
          "error",
          "Tải danh sách thất bại",
          error.message
        );
        setIsLoading(false);
      });
  };
  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
      message: message,
      description: description,
    });
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      render: (text) => <a href="# ">{text}</a>,
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
    },
    {
      title: "Người nhận",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
    },
    {
      title: "Nội dung",
      dataIndex: "smsContent",
      key: "smsContent",
    },

    {
      title: "Thời gian trễ",
      dataIndex: "smsDuration",
      key: "smsDuration",
    },
    {
      title: "Trạng thái",
      dataIndex: "isCompleted",
      key: "isCompleted",
      render: (text) => (
        <Tag
          color={text === true ? "success" : "red"}
          key={text === true ? "Đã gửi" : "Chưa gửi"}
        >
          {text === true ? "Đã gửi" : "Chưa gửi"}
        </Tag>
      ),
    },
  ];

  useEffect(() => {
    // getOne(pathId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <Row justify="start">
        <Title>Tìm kiếm tin nhắn</Title>
      </Row>

      <Col span={24}>
        <Search
      placeholder="Số điện thoại"
      enterButton="Search"
      size="large"
      style={{ paddingBottom: "10px" }}
      onSearch={value => {
        setIsLoading(true);
        findAllByPhoneNumber(value);
      }}
    />
      </Col>
      <Table
        rowKey="id"
        columns={columns}
        dataSource={projecTypes}
        pagination={false}
        loading={isLoading}
      />
    </div>
  );
};

export default SearchSMS;
