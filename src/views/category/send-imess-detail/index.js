import React, { useEffect, useState } from "react";
import {
  notification,
  Button,
  Table,
  Tag,
  Space,
  Row,
  Col,
  Modal,
  Typography,
  Breadcrumb,
} from "antd";
import axios from "axios";
import { host } from "../../../constant";
import { SyncOutlined } from "@ant-design/icons";
import { Link } from 'react-router-dom'
const SendImessDetail = () => {
  const [breadcrumbTitle, setBreadcrumbTitle] = useState("");
  const [projecTypes, setProjectTypes] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const path = window.location.pathname.split("/");
  const pathId = path[path.length - 1];
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const { Title } = Typography;

  const getOne = (id) => {
    setIsLoading(true);
    axios
      .get(`${host}/api/v1/single-content/getOne/${id}`)
      .then((res) => {
        setBreadcrumbTitle(res.data.mainContent.content)
        setProjectTypes(res.data.receivers || []);
        setIsLoading(false);
      })
      .catch((error) => {
        openNotificationWithIcon(
          "error",
          "Tải danh sách thất bại",
          error.message
        );
        setIsLoading(false);
      });
  };
  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
      message: message,
      description: description,
    });
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      render: (text) => <a href="# ">{text}</a>,
    },
    {
      title: "Người nhận",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
    },
    {
      title: "Nội dung",
      dataIndex: "smsContent",
      key: "smsContent",
    },

    {
      title: "Thời gian trễ",
      dataIndex: "smsDuration",
      key: "smsDuration",
    },
    {
      title: "Trạng thái",
      dataIndex: "isCompleted",
      key: "isCompleted",
      render: (text) => (
        <Tag
          color={text === true ? "success" : "red"}
          key={text === true ? "Đã gửi" : "Chưa gửi"}
        >
          {text === true ? "Đã gửi" : "Chưa gửi"}
        </Tag>
      ),
    }
    // {
    //   title: "Thao tác",
    //   key: "action",
    //   render: (text, record) => (
    //     <Space size="small">
    //       <Button
    //         onClick={() => {
    //           setSelectedId(record.id);
    //           showModal();
    //         }}
    //         type="primary"
    //         danger
    //       >
    //         Xoá
    //       </Button>
    //     </Space>
    //   ),
    // },
  ];

  const handleOk = () => {
    // setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  //   const onFinish = (values) => {
  //     callPut(values);
  //   };

  useEffect(() => {
    getOne(pathId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <Row justify="start">
        <Title>Chi tiết tin nhắn</Title>
      </Row>
      <Row>
      <Breadcrumb>
          <Breadcrumb.Item>
            <Link to="/">Trang chủ</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <Link to="/send-sms">Nội dung</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>{breadcrumbTitle}</Breadcrumb.Item>
        </Breadcrumb>
      </Row>
      <Row justify="end" style={{ paddingBottom: "10px" }}>
        <Col>
          <Space>
            <Button
              type="primary"
              onClick={() => getOne(pathId)}
              disabled={isLoading}
            >
              <SyncOutlined style={{ fontSize: "16px" }} spin={isLoading} />
              Tải lại
            </Button>
          </Space>
        </Col>
      </Row>
      <Table rowKey="id" columns={columns} dataSource={projecTypes} pagination={false} />

      {/* Modal Delete*/}
      <Modal
        title="Thông báo xác nhận"
        visible={visible}
        onOk={handleOk}
        okButtonProps={{ danger: true }}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        okText="Xoá"
        cancelText="Huỷ"
      >
        <p>Bạn có muốn xoá không</p>
      </Modal>

      {/* Modal create  */}
    </div>
  );
};

export default SendImessDetail;
