import React, { useEffect, useState } from "react";
import {
  notification,
  Button,
  Table,
  Space,
  Row,
  Col,
  Modal,
  Form,
  Input,
  Typography
} from "antd";

import { Link } from 'react-router-dom';
import axios from "axios";
import { host } from "../../../constant";
import { SyncOutlined, AppstoreAddOutlined } from "@ant-design/icons";

const Template = () => {
  const { TextArea } = Input;
  const [templates, setTemplates] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [isDialogLoading, setIsDialogLoading] = useState(false);
  const [selectedId, setSelectedId] = useState();
  const [dialogDeleteVisible, setDialogDeleteVisible] = useState(false);
  const [dialogCreateVisible, seteDialogCreateVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [form] = Form.useForm();
  const { Title } = Typography;


  // API 
  const getAll = () => {
    setIsLoading(true);
    axios
      .get(`${host}/api/v1/template`)
      .then((res) => {
        setTemplates(res.data || []);
        setIsLoading(false);
      })
      .catch((error) => {
        openNotificationWithIcon(
          "error",
          "Tải danh sách thất bại",
          error.message
        );
        setIsLoading(false);
      });
  };

  const callDelete = () => {
    axios
      .delete(`${host}/api/v1/template/${selectedId}`)
      .then((res) => {
        openNotificationWithIcon("success", "Xoá thành công", "");
        getAll();
      })
      .catch((error) => {
        openNotificationWithIcon("error", "Xoá thất bại", error.message);
      });
    setDialogDeleteVisible(false);
    setConfirmLoading(false);
  };

  const callPut = (data) => {
    setIsDialogLoading(true);
    axios
      .put(`${host}/api/v1/template`, data)
      .then((res) => {
        openNotificationWithIcon("success", "Thành công", "");
        getAll();
        seteDialogCreateVisible(false);
        setIsDialogLoading(false);
      })
      .catch((error) => {
        openNotificationWithIcon("error", "Thất bại", "Đã có lỗi xảy ra");
        seteDialogCreateVisible(false);
        setIsDialogLoading(false);
      });
    form.resetFields();
  };

  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
      message: message,
      description: description,
    });
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      render: (text) => <a href="# ">{text}</a>,
    },
    {
      title: "Nội dung",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Thao tác",
      key: "action",
      render: (text, record) => (
        <Space size="small">
          <Link to={`/template-detail/${record.id}`}>
            <Button type="primary">
              Chi tiết
            </Button>
          </Link>
          <Button
            type="primary"
            onClick={() => {
              onFill(record);
              showDialogModal();
            }}
          >
            Chỉnh sửa
          </Button>
          <Button
            onClick={() => {
              setSelectedId(record.id);
              showModal();
            }}
            type="primary"
            danger
          >
            Xoá
          </Button>
        </Space>
      ),
    },
  ];

  const showModal = () => {
    setDialogDeleteVisible(true);
  };

  const showDialogModal = () => {
    seteDialogCreateVisible(true);
  };

  const handleOk = () => {
    // setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);
    callDelete();
  };

  const handleCancel = () => {
    setDialogDeleteVisible(false);
  };

  const handleDialogCancel = () => {
    seteDialogCreateVisible(false);
    form.resetFields();
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 11,
      span: 11,
    },
  };

  const onFinish = (values) => {
    callPut(values);
  };


  const onFill = (data) => {
    form.setFieldsValue({
      id: data.id,
      name: data.name,
    });
  };

  // useEffect(() => {
  //   getAll();
  // }, [typeSearch]); // call after typeSearch state change

  useEffect(() => {
    getAll();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  return (
    <div>
      <Row justify="start">
        <Title>Cú pháp tin nhắn</Title>
      </Row>
      <Row justify="end" style={{ paddingBottom: "10px" }}>
        <Col>
          <Space>
            <Button type="primary" onClick={() => showDialogModal()}>
              <AppstoreAddOutlined style={{ fontSize: "16px" }} /> Thêm
            </Button>
            <Button
              type="primary"
              onClick={() => getAll()}
              disabled={isLoading}
            >
              <SyncOutlined style={{ fontSize: "16px" }} spin={isLoading} />
              Tải lại
            </Button>
          </Space>
        </Col>
      </Row>
      <Table
        rowKey="id"
        columns={columns}
        dataSource={templates}
        pagination={false}
      />

      {/* Modal Delete*/}
      <Modal
        title="Thông báo xác nhận"
        visible={dialogDeleteVisible}
        onOk={handleOk}
        okButtonProps={{ danger: true }}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        okText="Xoá"
        cancelText="Huỷ"
      >
        <p>Bạn có muốn xoá không</p>
      </Modal>

      {/* Modal create  */}
      <Modal
        title="Chỉnh sửa"
        visible={dialogCreateVisible}
        onCancel={handleDialogCancel}
        footer={null}
      >
        <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
          <Form.Item name="id" style={{ display: "none" }}>
            <Input />
          </Form.Item>
          <Form.Item
            name="name"
            label="Nội dung"
            rules={[{ required: true, message: "Không được để trống" }]}
          >
            <TextArea rows={10} />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit" loading={isDialogLoading}>
              Lưu
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default Template;
