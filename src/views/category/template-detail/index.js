import React, { useEffect, useState } from "react";
import {
  notification,
  Button,
  Table,
  Space,
  Row,
  Col,
  Modal,
  Typography,
  Breadcrumb,
  Form,
  Input
} from "antd";
import axios from "axios";
import { host } from "../../../constant";
import { SyncOutlined, AppstoreAddOutlined } from "@ant-design/icons";
import { Link } from 'react-router-dom'
const TemplateDetail = () => {
  const [templateDetails, setTemplateDetails] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [dialogVisible, seteDialogVisible] = useState(false);
  const [isDialogLoading, setIsDialogLoading] = useState(false);
  const path = window.location.pathname.split("/");
  const [selectedId, setSelectedId] = useState();
  const pathId = path[path.length - 1];
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const { Title } = Typography;
  const [form] = Form.useForm();
  const { TextArea } = Input;

  const getAll = (id) => {
    setIsLoading(true);
    axios
      .get(`${host}/api/v1/template-detail/getAllByTemplateId/${id}`)
      .then((res) => {
        setTemplateDetails(res.data || []);
        setIsLoading(false);
      })
      .catch((error) => {
        openNotificationWithIcon(
          "error",
          "Tải danh sách thất bại",
          error.message
        );
        setIsLoading(false);
      });
  };
  const openNotificationWithIcon = (type, message, description) => {
    notification[type]({
      message: message,
      description: description,
    });
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      render: (text) => <a href="# ">{text}</a>,
    },
    {
      title: "Nội dung",
      dataIndex: "templateContent",
      key: "templateContent",
    },
    {
      title: "Thao tác",
      key: "action",
      render: (text, record) => (
        <Space size="small">
          <Button
            type="primary"
            onClick={() => {
              onFill(record);
              showDialogModal();
            }}
          >
            Chỉnh sửa
          </Button>
          <Button
            onClick={() => {
              setSelectedId(record.id);
              showModal();
            }}
            type="primary"
            danger
          >
            Xoá
          </Button>
        </Space>
      ),
    },
  ];

  const showModal = () => {
    setVisible(true);
  };

  const showDialogModal = () => {
    seteDialogVisible(true);
  };

  const handleOk = () => {
    // setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);
    callDelete();
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const handleDialogCancel = () => {
    seteDialogVisible(false);
    form.resetFields();
  };

  const callDelete = () => {
    axios
      .delete(`${host}/api/v1/template-detail/${selectedId}`)
      .then((res) => {
        openNotificationWithIcon("success", "Xoá thành công", "");
        getAll(pathId);
      })
      .catch((error) => {
        openNotificationWithIcon("error", "Xoá thất bại", error.message);
      });
    setVisible(false);
    setConfirmLoading(false);
  };

  const callPut = (data) => {
    var request = {
      id: data.id,
      templateContent: data.templateContent,
      templateId: pathId
    };

    setIsDialogLoading(true);
    axios
      .put(`${host}/api/v1/template-detail`, request)
      .then((res) => {
        openNotificationWithIcon("success", "Thành công", "");
        getAll(pathId);
        seteDialogVisible(false);
        setIsDialogLoading(false);
      })
      .catch((error) => {
        openNotificationWithIcon("error", "Thất bại", "Đã có lỗi xảy ra");
        seteDialogVisible(false);
        setIsDialogLoading(false);
      });
    form.resetFields();
  };


  const onFill = (data) => {
    form.setFieldsValue({
      id: data.id,
      templateContent: data.templateContent,
      templateId: pathId
    });
  };

  const onFinish = (values) => {
    callPut(values);
  };

  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 11,
      span: 11,
    },
  };

  useEffect(() => {
    getAll(pathId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <Row justify="start">
        <Title>Chi tiết cú pháp</Title>
      </Row>
      <Row>
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link to="/">Trang chủ</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <Link to="/template">Cú pháp</Link>
          </Breadcrumb.Item>
        </Breadcrumb>
      </Row>
      <Row justify="end" style={{ paddingBottom: "10px" }}>
        <Col>
          <Space>
          <Button type="primary" onClick={() => showDialogModal()}>
              <AppstoreAddOutlined style={{ fontSize: "16px" }} /> Thêm
            </Button>
            <Button
              type="primary"
              onClick={() => getAll(pathId)}
              disabled={isLoading}
            >
              <SyncOutlined style={{ fontSize: "16px" }} spin={isLoading} />
              Tải lại
            </Button>
          </Space>
        </Col>
      </Row>
      <Table rowKey="id" columns={columns} dataSource={templateDetails} pagination={false} />

      {/* Modal create  */}
      <Modal
        title="Chỉnh sửa"
        visible={dialogVisible}
        onCancel={handleDialogCancel}
        footer={null}
      >
        <Form {...layout} form={form} name="control-hooks" onFinish={onFinish} initialValues={{templateContent:'{content}'}}>
          <Form.Item name="id" style={{ display: "none" }}>
            <Input />
          </Form.Item>
          <Form.Item name="templateId" style={{ display: "none" }}>
            <Input />
          </Form.Item>
          <Form.Item
            name="templateContent"
            label="Nội dung"
            rules={[{ required: true, message: "Không được để trống" }]}
          >
            <TextArea rows={10} />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit" loading={isDialogLoading}>
              Lưu
            </Button>
          </Form.Item>
        </Form>
      </Modal>

      {/* Modal Delete*/}
      <Modal
        title="Thông báo xác nhận"
        visible={visible}
        onOk={handleOk}
        okButtonProps={{ danger: true }}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        okText="Xoá"
        cancelText="Huỷ"
      >
        <p>Bạn có muốn xoá không</p>
      </Modal>

      {/* Modal create  */}
    </div>
  );
};

export default TemplateDetail;
